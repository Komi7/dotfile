<br>

<div align="justify">
<div align="center">


```ocaml
pxledit Dotfiles
```
</div>

<br>
<br>
<br>

## :snowflake: <samp>Information</samp>

<div>
<img src="https://archlinux.org/static/logos/archlinux-logo-dark-scalable.518881f04ca9.svg" width="400" height="200" align=right />

<table align=left><tr><td>
<b>- Operating System: </b><br />
<b>- Window Manager: </b><br />
<b>- Compositor: </b><br />
<b>- Terminal: </b><br />
<b>- Shell: </b><br />
<b>- Editor: </b><br />
<b>- GPU: </b><br /></table>

<table><tr><td>
<a href="https://archlinux.org">archlinux</a><br />
<a href="https://github.com/baskerville/bspwm">bspwm</a><br />
<a href="https://github.com/yshui/picom">Picom</a><br />
<a href="https://github.com/alacritty/alacritty">Alacritty</a><br />
<a href="https://www.zsh.org">ZSH</a><br />
<a href="https://neovim.io/">neovim</a><br />
<a href="https://www.nvidia.com">Nvidia</a><br /></table>
</div>

<br>
<br>






Auto-Installer:`my dot & package choice `
```shell
sudo curl -LO https://gitlab.com/Komi7/dotfile/-/raw/main/installscript ; bash installscript
```
~~extra link:~~

~~curl -Lks http://bit.ly/pxleditdotfile | bash~~

***


#### Package install [YAY]

```shell
 wget -O aur-list.pkg https://gist.githubusercontent.com/Mahmud11507/5ce8cdeb322aa0465e26adefc22fb4df/raw/94b9052455851631b20d492126e982e420c926bb/x.aur-list.pkg  &&
 yay -S - < aur-list.pkg
```
[raw view](https://gist.githubusercontent.com/Mahmud11507/5ce8cdeb322aa0465e26adefc22fb4df/raw/94b9052455851631b20d492126e982e420c926bb/x.aur-list.pkg)


<div align="center"> TODO  </div>
  
  
  ***
  
  - [x]  Working on perfect package list.
  - [ ]  fix script.


***

#### Sxhkd hotkey
- Terminal | super + Return  `termite/alacritty`

- Web | super + w  `brave`

- File-Manager | super + shift + Return `pcmanfm`

- Rofi  │ super + s

[More...](https://raw.githubusercontent.com/Komi7/dotfile/main/config/sxhkd/sxhkdrc)

#### Screenshot

`polybar`
> ![polybar](https://github.com/Pxledit/resources/blob/main/screenshot/2full.png)
`startpage`
> ![startpage](https://github.com/Pxledit/resources/blob/main/screenshot/braveinterface.png)
`nofetch`
> ![neofetch](https://github.com/Pxledit/resources/blob/main/screenshot/neofetch.png)
`rofi search theme`
> ![rofi search theme](https://github.com/Pxledit/resources/blob/main/screenshot/rofi-theme.png)
